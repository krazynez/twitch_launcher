## Setup

##### What you need
* [mpv](https://mpv.io/ "mpv homepage")
* [surf](https://surf.suckless.org/ "Surf Browser") *Browser*
* [streamlink](https://streamlink.github.io/install.html "Installation Guide")

## Running

`chmod +x twitch_launcher`

`./twitch_launcher`


### TODO's
Windows support
